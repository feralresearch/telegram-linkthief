class Database {
  init(db) {
    console.log("Database init...");
    db.serialize(function() {
      db.run(
        "CREATE TABLE if not exists links (link_id INTEGER PRIMARY KEY, datetime NUMERIC, userid NUMERIC, username TEXT, url BLOB, tags TEXT)"
      );
    });
  }
  queueForWrite(queue, payload) {
    queue.push(payload);
  }
  writeToDatabase(db, payload) {
    /*
    console.log(
      `${payload.datestamp} - ${payload.username} - ${payload.url} ${
        payload.tags !== null ? "-" + payload.tags : ""
      }`
  );*/

    db.serialize(function() {
      db.run(
        "INSERT INTO links(datetime,userid,username,url,tags) VALUES (?,?,?,?,?)",
        [
          payload.datestamp,
          payload.userid,
          payload.username,
          payload.url,
          payload.tags
        ]
      );
    });
  }
}

module.exports = Database;
