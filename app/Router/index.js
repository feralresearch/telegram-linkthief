const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const path = require("path");

module.exports = function(app, db, jsonParser) {
  let fields = ["link_id", "datetime", "username", "url", "tags"];
  let wwwroot = __dirname + "/../www";

  app.get("/nope", (req, res) => {
    res.sendFile(path.join(`${wwwroot}/nope.html`));
  });
  app.get("/style.css", (req, res) => {
    res.sendFile(path.join(`${wwwroot}/style.css`));
  });
  app.get("/script.js", (req, res) => {
    res.sendFile(path.join(`${wwwroot}/script.js`));
  });

  app.get("/:token?", (req, res) => {
    jwt.verify(req.params.token, process.env.JWT_SECRET, function(
      err,
      decoded
    ) {
      if (err) {
        res.sendFile(path.join(`${wwwroot}/nope.html`));
      } else {
        res.sendFile(path.join(`${wwwroot}/index.html`));
      }
    });
  });

  app.get("/api/links/:token", function(req, res) {
    jwt.verify(req.params.token, process.env.JWT_SECRET, function(
      err,
      decoded
    ) {
      if (err) return res.status(404).send();
      db.all("SELECT " + fields.join(", ") + " FROM links", function(
        err,
        rows
      ) {
        let retVal = { token: decoded, data: rows };
        res.status(200).send(retVal);
      });
    });
  });

  app.get("/api/reauthorize/:token", function(req, res) {
    jwt.verify(req.params.token, process.env.JWT_SECRET, function(
      err,
      decoded
    ) {
      if (err) return res.status(404).send();

      let newtoken = jwt.sign(
        { userid: decoded.userid, username: decoded.username },
        process.env.JWT_SECRET,
        {
          expiresIn: process.env.JWT_EXPIRES_IN
        }
      );
      res.status(200).send({ token: newtoken });
    });
  });
};
