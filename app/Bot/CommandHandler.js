const Telegraf = require("telegraf");
const Extra = require("telegraf/extra");
const Markup = require("telegraf/markup");
var jwt = require("jsonwebtoken");

class CommandHandler {
  handle(ctx) {
    let command = ctx.message.text
      .substring(1, ctx.message.text.length)
      .toLowerCase();
    switch (command) {
      case "stop":
        ctx.reply(`Can't stop, won't stop`);
        break;

      case "start":
        ctx.reply(`Hi! I will steal your links.`);
        break;

      case "links":
        const keyboard = Markup.inlineKeyboard([
          Markup.urlButton("", "http://127.0.0.1:3000/")
        ]);

        var token = jwt.sign(
          { userid: ctx.message.from.id, username: ctx.message.from.username },
          process.env.JWT_SECRET,
          {
            expiresIn: process.env.JWT_EXPIRES_IN
          }
        );
        ctx.reply(`${process.env.BOT_PUBLIC_LINK}/${token}`);
        break;
      default:
      //ctx.reply(`I cannot ${command} just because you want me to.`);
    }
  }
}
module.exports = CommandHandler;
