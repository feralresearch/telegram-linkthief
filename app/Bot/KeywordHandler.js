const getUrls = require("get-urls");
class KeywordHandler {
  handle(ctx, db, writeQueue, databaseMgr) {
    console.log(writeQueue);
    let linkSet = getUrls(ctx.message.text);
    if (linkSet.size > 0) {
      linkSet.forEach(link => {
        try {
          let payload = {
            datestamp: Date.now(),
            userid: ctx.message.from.id,
            username: ctx.message.from.username,
            url: link,
            tags: this.autoTag(link)
          };
          //console.log(JSON.stringify(payload));
          databaseMgr.queueForWrite(writeQueue, payload);
        } catch (e) {
          console.log(e);
          console.log("DATABASE PROBLEM!");
        }
      });
    }

    let text = ctx.message.text.toLowerCase();
  }

  autoTag(url) {
    url = url.toLowerCase();
    let tags = null;
    if (url.includes("youtube")) {
      tags = "youtube ";
    }
    return tags === null ? null : tags.trim();
  }
}
module.exports = KeywordHandler;
