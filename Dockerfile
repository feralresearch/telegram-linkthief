FROM node:8

# Create app directory
RUN mkdir /usr/src/app
WORKDIR /usr/src/app


# Bundle app source
COPY ./ .
RUN rm -rf ./data
RUN rm -rf ./node_modules
EXPOSE 8889

# Install app dependencies
ENV PATH /usr/src/node_modules/.bin:$PATH
COPY ./package*.json ./
RUN yarn install
CMD [ "node", "./app/index.js" ]
